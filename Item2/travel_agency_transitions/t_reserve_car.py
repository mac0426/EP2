from WEDWorker import WEDWorkerTemplate
import sys, random, time


class t_reserve_car(WEDWorkerTemplate):
    # trname and dbs variables are static in order to conform with the definition of wed_trans()
    trname = 't_reserve_car'
    dbs = 'user=wf_travel_agency dbname=wf_travel_agency host=127.0.0.1 application_name=t_reserve_car'
    wakeup_interval = 5

    def __init__(self):
        super().__init__(self.trname, self.dbs, self.wakeup_interval)

    # Compute the WED-transition and return a string as the new WED-state, using the SQL SET clause syntax
    # Return None to abort transaction
    def wed_trans(self, wed_state, wid):
        print("\033[33m> Reserving Car (Id = %d)\033[0m" % wid)
        print("\033[33m> WED-state: %s\033[0m" % wed_state)

        # business logic goes here
        time.sleep(5)
        print(time.strftime("%d/%m/%Y %H:%M:%S"))
        return "car_status = 'Reserved'"


w = t_reserve_car()

try:
    w.run()
except KeyboardInterrupt:
    print()
    sys.exit(0)

